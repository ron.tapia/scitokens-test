#!/usr/bin/env perl
use strict;
use warnings;
use FileHandle;
use Cwd qw(getcwd);

my($credDir) = $ENV{'_CONDOR_CREDS'};
if (defined($credDir)) {
    printf("---------------------------\n");
    printf("stat %s:\n", $credDir);
    system(sprintf("stat %s 2>&1", $credDir));
    printf("ls -al %s:\n", $credDir);
    system(sprintf("ls -al %s 2>&1", $credDir));
    printf("---------------------------\n");
    if (-d $credDir) {
        $ENV{'BEARER_TOKEN_FILE'} = sprintf("%s/ligo.use", $credDir );
        printf("setting BEARER_TOKEN_FILE=%s/ligo.use\n", $credDir );
    } elsif (-f $credDir) {
        printf("credDir is actually a file...\n");
        printf("---------------------------\n");
        system(sprintf("cat %s", $credDir));
        printf("---------------------------\n");
        $ENV{'BEARER_TOKEN_FILE'} = $credDir;
        printf("setting BEARER_TOKEN_FILE=%s\n", $credDir );
    }
}
# Fall back to well known location...
my($standardTokenFile) = sprintf("%s/.condor_creds/ligo.use", getcwd());
if (!defined($ENV{'BEARER_TOKEN_FILE'})) {
    printf("Checking to see if token is in: %s\n", $standardTokenFile);
    if ( -f $standardTokenFile) {
        $ENV{'BEARER_TOKEN_FILE'} = $standardTokenFile;
        printf("setting BEARER_TOKEN_FILE=%s\n", $standardTokenFile);
        printf("---------------------------\n");
        system(sprintf("cat %s", $standardTokenFile));
        printf("---------------------------\n");
    }
}

printf("whoami: ");
system("whoami");

printf("Hostname: " . `hostname -f`);
if (@ARGV) {
    printf("ARGS: (\"%s\")\n", join("\", \"", @ARGV))
} else {
    printf("ARGS: ()\n");
}
my($outDir) = "unknown";
if (defined($ARGV[0])) {
    $outDir = $ARGV[0];
}
system("mkdir $outDir");
printf("PWD: " . `pwd`);
my($rh) = "UNKNOWN";
if (-f '/etc/redhat-release') {
    my($rh) = `cat /etc/redhat-release`;
    chomp($rh);
}
printf("redhat release: %s\n", $rh);


###
###
###
my($bearerTokenFileVar) = $ENV{'BEARER_TOKEN_FILE'};
if (defined($bearerTokenFileVar)) {
    printf("BEARER_TOKEN_FILE=%s\n", $bearerTokenFileVar);
    if (-f $bearerTokenFileVar) {
        printf("BEARER_TOKEN_FILE exists and is a file.\n");
    } else {
        printf("BEARER_TOKEN_FILE does not exist or is not a file.\n");
    }
} else {
    printf("BEARER_TOKEN_FILE is NOT set.\n");
}

###
###
###
writeEnv($outDir);
writeDf($outDir);
writeIfconfig($outDir);
writeFree($outDir);
writeFind($outDir);
writeTokens($outDir);
system(sprintf("cp %s %s", $ENV{'_CONDOR_MACHINE_AD'}, getFname('machine.ad', $outDir)));
system(sprintf("cp %s %s", $ENV{'_CONDOR_JOB_AD'}, getFname('job.ad', $outDir)));
system(sprintf("ls -al /run/user > %s", getFname("ls.run.user.txt", $outDir)));
#runGstlal($outDir);
writeCpuInfo($outDir);
runFrCheck($outDir);
#printf("Sleeping...\n");
#sleep(600);
#printf("Finished Sleeping...\n");

exit(0);
###
### 
###
sub getFname {
    my($n)    = shift;
    my($outDir) = shift;
    return sprintf("%s/%s", $outDir, $n);
}

sub writeEnv {
    my($outDir) = shift;
    my($fname) = getFname('env.txt', $outDir);
    system("env > $fname");
}

sub writeDf {
    my($outDir) = shift;
    my($fname) = getFname('df.txt', $outDir);
    system("df -h > $fname");
}

sub writeIfconfig {
    my($outDir) = shift;
    my($fname) = getFname('ifconfig.txt', $outDir);
    if ( -f '/sbin/ifconfig') {
        system("/sbin/ifconfig -a > $fname");
    } else {
        system("touch $fname");
    }
}

sub writeFree {
    my($outDir) = shift;
    my($fname) = getFname('free.txt', $outDir);
    if (-f '/usr/bin/free') {
        system("/usr/bin/free > $fname");
    } else {
        system("touch $fname");
    }
}

sub writeCpuInfo {
    my($outDir) = shift;
    my($fname) = getFname('cpuinfo.txt', $outDir);
    system("cat /proc/cpuinfo > $fname");
}

sub writeFind {
    my($outDir) = shift;
    my($fname) = getFname('find.txt', $outDir);
    system("find . > $fname");    
}

sub runGstlal {
    my($outDir) = shift;
    my($outPsd) = getFname('out.psd', $outDir);
    printf("Create %s\n", $outPsd);
    my($ldpath) = '/opt/intel/compilers_and_libraries/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries/linux/tbb/lib/intel64_lin/gcc4.7:/opt/intel/compilers_and_libraries/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries/linux/mkl/lib/intel64_lin:/.singularity.d/libs';
    $ENV{'LD_LIBRARY_PATH'} = sprintf("%s:%s", $ldpath, $ENV{'LD_LIBRARY_PATH'});
    my($g, $command);
    my($args) = sprintf(" --channel-name=H1=foo --data-source=white  --write-psd=%s --gps-start-time=1185493488 --gps-end-time=1185493788", $outPsd);
    if ( -f ($g='/bin/gstlal_reference_psd')) {
        $command = sprintf("%s %s", $g, $args);
    } elsif (-f ($g='/usr/bin/gstlal_reference_psd')) {
        $command = sprintf("%s %s", $g, $args);
    } else {
        $command = "echo foo > $outPsd";
    }
    if (! -f $outPsd) {
        system("echo FAIL > $outPsd");
    }
    printf("RUNNING command: %s\n", $command);
    my($exitCode) = system($command);
    if ($? == -1) {
        print "command failed to execute\n";
    } elsif ($? & 127) {
        printf "child died with signal %d, %s coredump\n",
        ($? & 127),  ($? & 128) ? 'with' : 'without';
    } else {
        printf "child exited with value %d\n", $? >> 8;
    }
    printf("FINISHED: %s\n", $command);
}

sub runFrCheck {
    my($outDir) = shift;
#    my($frameFile) = "/cvmfs/oasis.opensciencegrid.org/ligo/frames/O3/hoft/H1/H-H1_HOFT_C00-12692/H-H1_HOFT_C00-1269264384-4096.gwf";
    my($frameFile) = "/cvmfs/ligo.osgstorage.org/frames/O3/hoft/H1/H-H1_HOFT_C00-12692/H-H1_HOFT_C00-1269264384-4096.gwf";
    my($command) = sprintf("ls -al %s > %s 2>&1", $frameFile, getFname('lsframe.out', $outDir));
    system($command);
#    $command = sprintf("/usr/bin/FrCheck -i %s > %s 2>&1", $frameFile, getFname('frcheck.out', $outDir));
    $command = sprintf("echo 'Expecting: ad95bd2b2da3e85e473ae327b604d421' > %s", getFname('frcheck.out', $outDir));
    system($command);
    $command = sprintf("head -c 4K %s | md5sum >> %s", $frameFile, getFname('frcheck.out', $outDir));
    printf("RUNNING command: %s\n", $command);
    my($exitCode) = system($command);
    if ($? == -1) {
        print "command failed to execute\n";
    } elsif ($? & 127) {
        printf "child died with signal %d, %s coredump\n",
        ($? & 127),  ($? & 128) ? 'with' : 'without';
    } else {
        printf "child exited with value %d\n", $? >> 8;
    }
    printf("FINISHED: %s\n", $command);
}

sub writeTokens {
    my($outDir) = shift;
    my($credDir) = $ENV{'_CONDOR_CREDS'};
    my($fh) = FileHandle->new($outDir . "/tokens.txt", "w");
    if (!defined($credDir)) {
        printf($fh "_CONDOR_CREDS is not defined.\n");
        $fh->close();
        return -1;
    }
    if (! -d $credDir) {
        printf($fh "_CONDOR_CREDS is not a directory.\n");
        $fh->close();
        return -1;
    }
    my($dh);
    opendir($dh, $credDir);
    my($f);
    my($th);
    my($line);
    my($tokenPath);
    while ($f = readdir($dh)) {
        $tokenPath = sprintf("%s/%s", $credDir, $f);
        if (-f $tokenPath) {
            printf($fh "===================================\n");
            printf($fh "FILE: %s\n", $f);
            printf($fh "===================================\n");
            $th = FileHandle->new($tokenPath, "r");
            while ($line = <$th>) {
                printf($fh "%s", $line);
            }
            $th->close();
        }
    }
}
