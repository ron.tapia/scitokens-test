# scitokens-test

A simple test of the ability to condor_submit a job that uses SciTokens.

This repository contains four condor submit files and a test program.

## Submit files

The submit files differ in whether they set:
```
  use_oauth_services=ligo

```
and whether they set the requirement `IS_GLIDEIN`.

The submit files without `use_oauth_services=ligo` have `shouldfail` in the file name and should fail to read frame files when submitted. They should also not prompt for an OIDC workflow.

The submit files with `IS_GLIDEIN` have `osg` in their file name. Note that the OSG submit files will try to run the job and 

| Submit file | Should succeed  | Success indicates | Failure indicates |
|:---  |:---: |:--- |:--- |
|test-local.sub | Y | LIGO Scitokens support is configured correctly for local submission | LIGO Scitokens support is not configured correctly for local submission | 
| test-local-shouldfail.sub | N | Authentication (x509 or SciToken) is not necessary to read frame files | Normal, job with no credentials cannot read frame files |
|test-osg.sub | Y | LIGO Scitokens support is configured correctly for OSG submission | LIGO Scitokens support is not configured correctly for OSG submission | 
|test-osg-shouldfail.sub | N | Authentication (x509 or SciToken) is not necessary to read frame files | Normal, job with no credentials cannot read frame files |

## Test Program

The test program is `testprog.pl`. It writes to stdout and writes several files in a directory that is named as it first argument.

## The environment file

A sample environment file, `env.sh-sample`, is included in the repo. It contains two environment variables that must be set in order for submission to work.
The variables should be familiar to users submitting jobs on IGWN hosts.

## What to expect

The first time that you submit a job using SciTokens, you will be asked to go through the OIDC workflow (put a URL into a browser). This will generate a SciToken and a "Vault token". It will print the file system paths to the files containing the tokens. If you want to force the OIDC workflow, delete both tokens.

Once you have a token, the job should be submitted as normal. The job shouldn't take very long to run. When the job is finished running you should expect to see `.out`, `.log`, and `.err` files in the submit directory and a similarly named directory containing the files written by the test program.

## Success or Failure

To see if a job using SciTokens can read frame files, look at the file in the output directory named `frcheck.out`. It contains two
lines. The first is always the same and it contains the expected result. The second contains a checksum follwed by a dash. If the
checksum matches the expected checksum, the script was able to read a frame file. That is, it was successful.

A convenience script named `isSuccess.sh` is provided. It can be run with an output directory as an argument. It will 
print "YES" and exit with value 0 on success. On failure it will print "NO" and exit with value 1.

## Getting started

1. Copy `env.sh-sample` to `env.sh`, edit it, and source it.
2. Submit a job using `condor_submit`. 

You should submit the `shouldfail` job at least once to verify that it does indeed fail, otherwise
the test isn't testing very much.

A complete test should look something like:
```
git clone git@git.ligo.org:ron.tapia/scitokens-test.git local.pcdev8.20220616.1
cd local.pcdev8.20220616.1
# edit env.sh
. env.sh
condor_submit test-local.sub
condor_submit test-local-shouldfail.sub
./isSuccess.sh local-out-44276-0
./isSuccess.sh local-fail-out-44277-0
```
